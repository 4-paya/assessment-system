import React from 'react'
import { Dropdown } from 'semantic-ui-react'

const friendOptions = [
  {
    key: 'project A',
    text: 'Project A',
    value: 'Jenny Hess'
  },
  {
    key: 'project B',
    text: 'Project B',
    value: 'Elliot Fu'
  },
  {
    key: 'Project C',
    text: 'project A',
    value: 'Stevie Feliciano'
  },
  {
    key: 'Christian',
    text: 'โปคเจ็ค D',
    value: 'Christian'
  },
  {
    key: 'Matt',
    text: 'โปคเจ็ค E',
    value: 'Matt'
  },
  {
    key: 'Justen Kitsune',
    text: 'โปคเจ็ค F',
    value: 'Justen Kitsune'
  },
]

const DropdownExampleSelection = () => (
  <Dropdown
    placeholder='ชื่อโปรเจค'
    fluid
    selection
    options={friendOptions}
  />
)

export default DropdownExampleSelection