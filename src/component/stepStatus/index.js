import React from 'react';
import { Button } from '@material-ui/core';
import { Steps, ButtonGroup } from 'rsuite';
import { makeStyles } from '@material-ui/core/styles';
import "rsuite/dist/rsuite.css";
import { CCard } from "@coreui/react";
import Step1 from './step1';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  divMain: {
    backgroundColor: '#153D77',
    fontFamily: 'Prompt',
    color: '#FFFFFF',
    borderRadius: 5,
    height: 40
  },
  button: {
    backgroundColor: '#2F80ED',
    color: '#FFFFFF',
    borderRadius: 35,
    fontFamily: 'Prompt',
    marginLeft: 20,
  },
  stepButton: {
    color: 'rgb(50, 180, 109)',
  }
}));

const StepStatus = () => {
  const classes = useStyles();
  const [step, setStep] = React.useState(0);
  const onChange = nextStep => { setStep(nextStep < 0 ? 0 : nextStep > 7 ? 7 : nextStep) };
  const onNext = () => onChange(step + 1);
  const onPrevious = () => onChange(step - 1);

  return (
    <div>
      <Steps current={step} className={classes.stepButton}>
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
        <Steps.Item />
      </Steps>
      <hr />

      {/* แบบประเมิน */}
      <div className={classes.root}>
        <CCard>
          <Step1 />
          {/* ปุ่ม กลับ/ถัดไป */}
          <ButtonGroup style={{ float: 'right' }}>
            <Button onClick={onPrevious} disabled={step === 0} className={classes.button} >
              กลับ
            </Button>
            <Button onClick={onNext} disabled={step === 7} className={classes.button}>
              ถัดไป
            </Button>
          </ButtonGroup>
        </ CCard>
      </div>
    </div>
  );
};

export default StepStatus;