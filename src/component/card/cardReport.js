import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { CCard, CCardHeader, CCardBody, CCardText } from "@coreui/react";
import TableReport from "../table/tableReport";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    divMain: {
        backgroundColor: '#153D77',
        fontFamily: 'Prompt',
        color: '#FFFFFF',
        borderRadius: 5,
        height: 40
    },
    div: {
        backgroundColor: '#FFFFFF',
        height: 'calc(100vh - 250px)'
    }
}));


const CardReport = () => {
    const classes = useStyles();

    return (
        <CCard>
        <div className={classes.divMain} >
            <CCardHeader style={{ lineHeight: 2, marginLeft: 30, fontSize: 18 }}>ส่วนที่ 3: ผลการประเมิน</CCardHeader>
        </div>
        <div className={classes.div}>
            <CCardBody>
                <CCardText style={{ lineHeight: 2, marginLeft: 30, fontSize: 18, fontFamily: 'Prompt' }}>1. คุณภาพ : ผลงานหรืองานที่ทำ มีความละเอียด ถูกต้อง ครบถ้วน ได้มาตารฐาน ไม่มีการแก้ไขหรือปรับปรุง</CCardText>
                <TableReport />
                </CCardBody>
            </div>
        </CCard>
    )
}

export default CardReport;