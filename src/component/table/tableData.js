import React, { useState } from 'react';
import 'antd/dist/antd.css';
import './index.css';
import { Table } from 'antd';

const columns = [
  {
    title: 'ชื่อ - นามสกุล',
    dataIndex: 'name',
    sorter: (a, b) => a.codeId - b.codeId,
  },
  {
    title: 'รหัสพนักงาน',
    dataIndex: 'codeId',
    sorter: (a, b) => a.codeId - b.codeId,
  },
  {
    title: 'ตำแหน่ง',
    dataIndex: 'position',
    sorter: (a, b) => a.codeId - b.codeId,
  }
];

const data = [];

for (let i = 1; i <= 5; i++) {
  data.push({
    key: i,
    name: Number(`${i}`) + ') ณัฐกร ทำดี',
    codeId: Number(`3045${i}`),
    position: `xxxx No. ${i}`,
  });
}

const defaultTitle = () => 'Here is title';
const TableData = () => {
  const [bordered, setBordered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [size, setSize] = useState('large');
  const [showTitle, setShowTitle] = useState(false);
  const [showHeader, setShowHeader] = useState(true);
  const [hasData, setHasData] = useState(true);
  const [tableLayout, setTableLayout] = useState(undefined);
  const [top, setTop] = useState('none');
  const [bottom, setBottom] = useState('bottomRight');
  const [ellipsis, setEllipsis] = useState(false);
  const tableColumns = columns.map((item) => ({ ...item, ellipsis }));

  const tableProps = {
    bordered,
    loading,
    size,
    title: showTitle ? defaultTitle : undefined,
    showHeader,
    tableLayout,
  };
  
  return (
    <>
      <Table
        {...tableProps}
        pagination={{
          position: [top, bottom],
        }}
        columns={tableColumns}
        dataSource={hasData ? data : []}
      />
    </>
  );
};

export default TableData;