import { Routes, Route } from 'react-router-dom';
import ReportDetail from '../page/Report';
import Dashboard from '../page/Dashboard';
import MainPotential from '../page/Potential';
import MainPerformance from '../page/Performance';
import FormPotential from '../page/Potential/FormPotential';


const RoutePath = () => {
    return (
        <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/potential" element={<MainPotential />} />
            <Route path="/performance" element={<MainPotential />} />
            <Route path="/potential/report" element={<ReportDetail />} />
            <Route path="/performance/report" element={<ReportDetail />} />
            <Route path="/potential/assessment" element={<FormPotential />} />
            <Route path="/performance/assessment" element={<MainPerformance />} />
        </Routes>
    )
}

export default RoutePath;