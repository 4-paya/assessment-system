import React from 'react';
import './App.css';
import Signin from './page/Signin';
import Sidebar from './component/sidebar/sidebar';

function App() {
  const token = localStorage.getItem('accessToken');

  if (!token) {
    return <Signin />
  }

  return (
    <div className="wrapper">
      <Sidebar />
    </div>
  );
}

export default App;
