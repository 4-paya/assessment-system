import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import CardReport from "../component/card/cardReport";


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    media: {
        height: '90vh',
    },
    large: {
        width: theme.spacing(20),
        height: theme.spacing(20),
    },
    text: {
        fontFamily: 'Prompt',
    }
}));

const ReportDetail = () => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
            </AppBar>
            <Typography className={classes.text} variant="h5">
                ผลการประเมิน Potential : ชื่อโปรเจค
            </Typography>
            <CardContent style={{ padding: 30 }}>
                <CardReport />
            </CardContent>
        </div>
    )
};

export default ReportDetail;