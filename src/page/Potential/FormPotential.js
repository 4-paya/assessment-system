import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import StepStatus from '../../component/stepStatus';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    text: {
        fontFamily: 'Prompt',
    }
}));

const  FormPotential = () => 
{
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    return (
        <div className={classes.root}>
            <AppBar position="static">
            </AppBar>
            <Typography className={classes.text} variant="h5">
                แบบประเมิน Potential : ชื่อโปรเจค
            </Typography>
            <CardContent style={{ padding: 30 }}>
                <StepStatus />
            </CardContent>
        </div>
    );
}

export default FormPotential;